# DotNet 5 Version Auto-Increment

A powershell script that can be used to auto-increment, or overwrite, the version number of your .NET 5 project JSON file.

Original auto-increment code was written by [GuardRex](https://github.com/GuardRex/net5-autoupdate-version) over at GitHub. I have since added optional parameters so the powershell script may be placed wherever you please, and/or run against files with different names, to better suit my Continuous Integration needs.

## Requirements

* .NET 5 Project
* PowerShell 4+

### Parameters (optional)
* -version [string]
  * If used, will completely overwrite the version string.
  * May break build if invalid version string is entered. (No validation.)
* -configDir [string]
  * Path to the configuration directory. If left empty, the script will use its own directory as target.
* -fileName [string]
  * If you want to run the PowerShell script against any other JSON file, that has a "version" property, you can change the name this way.

## Example

You can script the powershell to execute - and therefore bump your version number - before publishing the content, by adding a `prepublish` node to your `project.json`

```
"scripts": {
    "prepublish": "powershell -ExecutionPolicy ByPass -file C:\\temp\\buildTools\\versioning.ps1 -configDir %project:Directory%"
}
```
